/* 
 * File:   main.cpp
 * Author: korolko2
 *
 * Created on November 10, 2015, 7:05 PM
 */

#include <cstdlib>
#include <iostream>
#include "Field.h"

using namespace std;

int main(int argc, char** argv) {
    // Setting up the board
    int input = atoi(argv[1]);
    if (input % 2) {
        cout << "ERROR: Cannot use odd sized board" << endl;
    } else {
        Field board(atoi(argv[1]));
        // Adding units:
        board.putEnemyOnField(8,8);
        board.printHeuristic();
        board.findEnemy();
    }
    return 0;
}