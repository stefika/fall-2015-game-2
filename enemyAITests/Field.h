/* 
 * File:   Field.h
 * Author: korolko2
 *
 * Created on November 10, 2015, 7:06 PM
 */

#ifndef FIELD_H
#define	FIELD_H

#include <cstdlib>
#include <iostream>

using namespace std;

struct Arena {
    int hp;
    int heuristic;
    string type;
    char ID;
};

class Field {
public:
    Field(int sizeBoard);
    void printCurrentBoard();
    void printHeuristic();
    void putEnemyOnField(int x, int y);
    void putFriendlyOnField(int x, int y);
    void findEnemy();
    int quadrantID(int x, int y);
private:
    void setupBoard();
    void setupConstants();
    void findPath(int x, int y);

    Arena **board;
    Arena grass, dirt, keep;
    Arena friendlyUnit, enemyUnit;
    int size, keepLocation;
    bool even;
};

#endif	/* FIELD_H */

