// Binary Heap Data Structure
// Priority Queue (min > max)
use Libraries.Containers.Array
class BinaryHeap 
    integer count   = 0    
    Array<Coord> coord
        
    on create
        Coord zero
        zero:X = -1
        zero:Y = -1
        zero:W = -1
        coord:Add(zero)
    end

    action IsEmpty returns boolean
        if(count = 0)
            return true
        end
        return false         
    end

    action Insert(Coord c)
        count = count + 1
        coord:Add(c)
        ReheapUp(count)
    end
    
    action ReheapUp(integer last) returns integer
        if(last = 1)                
            return 0
        end
        Coord tmp 
        tmp:X = coord:Get(last):X
        tmp:Y = coord:Get(last):Y
        tmp:W = coord:Get(last):W
        Coord parentCoord = coord:Get(last/2)
        if(tmp:W < parentCoord:W)
            coord:Set(last, parentCoord)
            coord:Set(last/2, tmp)
            ReheapUp(last/2)
        end
        return 0
    end
    
    private action ReheapDown(integer in) returns integer        
        if(in = count or 2 * in >= count)
            return 0
        end
        Coord tmp
        integer max = 0

        if(2*in+1 >= count)
            max = 2 * in
        else
            Coord childL = coord:Get(2*in)
            Coord childR = coord:Get(2*in+1)
            if(childL:W <= childR:W)
                max = 2 * in
            else
                if(childR:W > 0)
                    max = 2 * in + 1
                else
                    return 0
                end
            end
        end
        
        if (max >= count or max < 1)
            return 0
        end
        
        if (coord:Get(in):W >= coord:Get(max):W)
            tmp:X = coord:Get(in):X
            tmp:Y = coord:Get(in):Y
            tmp:W = coord:Get(in):W
            coord:Set(in, coord:Get(max))
            coord:Set(max, tmp)
            ReheapDown(max)
        end
        return 0
    end 
    
    private action BuildHeap()
        integer i = count/2
        repeat while  i > 0
            ReheapDown(i)
            i = i - 1
        end
    end 

    action ExtractMax() returns Coord
        if(count = 0)
            return coord:Get(0)
        end
        return coord:Get(1)        
    end
    
    action PopMax() returns Coord
        if(count = 0)
            return coord:Get(0)
        end
        Coord tmp 
        tmp:X = coord:Get(1):X
        tmp:Y = coord:Get(1):Y
        tmp:W = coord:Get(1):W        
        Coord new        
        new:X = coord:Get(count):X
        new:Y = coord:Get(count):Y
        new:W = coord:Get(count):W
        coord:Set(1, new)
        coord:RemoveFromEnd()                        
        count = count - 1
        if(count > 1)
            ReheapDown(1)
        end
        return tmp        
    end

    action OutputHeap()
        integer size = coord:GetSize()
        integer i = 0
        output "BinaryHeap"
        repeat while i < size
            Coord t = coord:Get(i)
            output " i:" + i + " => " + t:X + " " + t:Y + " " + t:W
            i = i + 1
        end
        output "end"
    end
end