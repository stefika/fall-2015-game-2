/* 
 * File:   Field.cpp
 * Author: korolko2
 * 
 * Created on November 10, 2015, 7:06 PM
 */

#include "Field.h"

Field::Field(int sizeBoard) {
    size = sizeBoard;
    setupConstants();
    setupBoard();
}

void Field::setupConstants() {
    // setup Grass;
    grass.type = "grass";
    grass.ID = 'g';
    grass.hp = 0;

    // setup Dirt
    dirt.type = "dirt";
    dirt.ID = 'd';
    dirt.hp = 0;

    // setup Keep
    keep.type = "keep";
    keep.ID = 'k';
    keep.hp = 10;

    // setup Enemy Unit
    enemyUnit.type = "Bad Guy";
    enemyUnit.ID = 'e';
    enemyUnit.hp = 5;

    // setup Friendly Unit
    friendlyUnit.type = "Good Guy";
    friendlyUnit.ID = 'f';
    friendlyUnit.hp = 7;
}

void Field::setupBoard() {
    board = new Arena*[size];
    for (int i = 0; i < size; i++) {
        board[i] = new Arena[size];
    }

    for (int i = 0; i < size; i++) {
        if (i == 0 || i == 1 || i == 2 || i == (size - 2) || i == (size - 1) || i == size) {
            for (int j = 0; j < size; j++) {
                board[i][j] = dirt;
            }
        } else {
            for (int j = 0; j < size; j++) {
                if (j == 0 || j == 1 || j == 2 || j == (size - 2) || j == (size - 1) || j == size) {
                    board[i][j] = dirt;
                } else {
                    board[i][j] = grass;
                }
            }
        }

    }

    if (size % 2) {
        // One Square
        even = false;
        keepLocation = size / 2;
        board[keepLocation][keepLocation] = keep;
    } else {
        // Four Squares
        even = true;
        keepLocation = size / 2;
        board[keepLocation][keepLocation] = keep;
        board[keepLocation][keepLocation - 1 ] = keep;
        board[keepLocation - 1][keepLocation] = keep;
        board[keepLocation - 1][keepLocation - 1 ] = keep;
    }
    int temp, tempx, tempy;
    for (int i = 0; i < size; i++) {
        for (int j = 0; j < size; j++) {
            if (i <= keepLocation - 1 && j <= keepLocation - 1) { //First Quadrant
                //                board[i][j].heuristic = 1;
                tempx = (keepLocation - 1) - i;
                tempy = (keepLocation - 1) - j;
                if (tempx < tempy) {
                    temp = tempy;
                } else {
                    temp = tempx;
                }
                board[i][j].heuristic = temp;
            } else if (i >= keepLocation - 1 && j <= keepLocation - 1) { //Third Quadrant
                tempx = i - (keepLocation);
                tempy = (keepLocation - 1) - j;
                if (tempx < tempy) {
                    temp = tempy;
                } else {
                    temp = tempx;
                }
                board[i][j].heuristic = temp;
            } else if (i <= keepLocation - 1 && j >= keepLocation - 1) { //Second Quadrant
                tempx = (keepLocation - 1) - i;
                tempy = j - (keepLocation);
                if (tempx < tempy) {
                    temp = tempy;
                } else {
                    temp = tempx;
                }
                board[i][j].heuristic = temp;
            } else if (i >= keepLocation - 1 && j >= keepLocation - 1) { //Fourth Quadrant
                tempx = i - (keepLocation);
                tempy = j - (keepLocation);
                if (tempx < tempy) {
                    temp = tempy;
                } else {
                    temp = tempx;
                }
                board[i][j].heuristic = temp;
            } else {
                cout << "ERROR" << endl;
            }

        }
    }
}

void Field::printCurrentBoard() {
    cout << "Field |";
    for (int i = 0; i < size; i++) {
        if (i < 10) {
            cout << " " << i << " |";
        } else {
            cout << i << " |";
        }
    }
    cout << endl;
    for (int i = 0; i < size; i++) {
        cout << "------|";
        for (int j = 0; j < size; j++) {
            cout << "---|";
        }
        cout << endl;
        if (i < 10) {
            cout << "    " << i << " |";
        } else {
            cout << "   " << i << " |";
        }
        for (int j = 0; j < size; j++) {
            cout << " " << board[i][j].ID << " |";
        }
        cout << endl << "      |";
        for (int j = 0; j < size; j++) {
            if (board[i][j].hp < 0) { // Less than Zero
                cout << board[i][j].hp << " |";
            } else if (board[i][j].hp > 9) { //Greater than 9
                cout << board[i][j].hp << " |";
            } else { // 0--
                cout << " " << board[i][j].hp << " |";
            }
        }
        cout << endl;
    }
    cout << endl;
}

void Field::printHeuristic() {
    cout << "Field |";
    for (int i = 0; i < size; i++) {
        if (i < 10) {
            cout << " " << i << " |";
        } else {
            cout << i << " |";
        }
    }
    cout << endl;
    for (int i = 0; i < size; i++) {
        cout << "------|";
        for (int j = 0; j < size; j++) {
            cout << "---|";
        }
        cout << endl;
        if (i < 10) {
            cout << "    " << i << " |";
        } else {
            cout << "   " << i << " |";
        }
        for (int j = 0; j < size; j++) {
            if (board[i][j].ID == 'd' || board[i][j].ID == 'g') {
                cout << "   |";
            } else {
                cout << " " << board[i][j].ID << " |";
            }
        }
        cout << endl << "      |";
        for (int j = 0; j < size; j++) {
            if (board[i][j].ID == 'f' || board[i][j].ID == 'e' || board[i][j].ID == 'k') {
                if (board[i][j].hp < 0) { // Less than Zero
                    cout << board[i][j].hp << " |";
                } else if (board[i][j].hp > 9) { //Greater than 9
                    cout << board[i][j].hp << " |";
                } else { // 0--
                    cout << " " << board[i][j].hp << " |";
                }
            } else {
                if (board[i][j].heuristic < 0) { // Less than Zero
                    cout << board[i][j].heuristic << " |";
                } else if (board[i][j].heuristic > 9) { //Greater than 9
                    cout << board[i][j].heuristic << " |";
                } else { // 0--
                    cout << " " << board[i][j].heuristic << " |";
                }
            }
        }
        cout << endl;
    }
    cout << "------|";
    for (int j = 0; j < size; j++) {
        cout << "---|";
    }
    cout << endl;
}

void Field::putEnemyOnField(int x, int y) {
    board[x][y] = enemyUnit;
}

void Field::putFriendlyOnField(int x, int y) {
    board[x][y] = friendlyUnit;
}

void Field::findEnemy() {
    bool enemyExist = false;
    int x, y;
    for (int i = 0; i < size; i++) {
        for (int j = 0; j < size; j++) {
            if (board[i][j].ID == 'e') {
                enemyExist = true;
                x = i;
                y = j;
                break;
            }
        }
    }
    if (enemyExist) {
        findPath(x, y);
    } else {
        cout << "No enemies on field" << endl;
    }
}

int Field::quadrantID(int x, int y) {
    int half = keepLocation -1;

    if (x <= half && y <= half){
        return 1;
    } else if (x <= half && y >= half) {
        return 2;
    } else if (x >= half && y <= half) {
        return 3;
    } else if (x > half && y > half) {
        return 4;
    }
}

void Field::findPath(int x, int y){
    // find which Quadrant the enemy should move
    switch(quadrantID(x, y)){
        case 1: {
            cout << 1 << endl;
            break;
        }
        case 2: {
            cout << 2 << endl;
            break;
        }
        case 3: {
            cout << 3 << endl;
            break;
        }
        case 4: {
            cout << 4 << endl;
            break;
        }
    } 
}

